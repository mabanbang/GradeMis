﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GradeMis
{
    public partial class AdminGrade : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["GradeSys"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            // 在此处放置用户代码以初始化页面
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }
        protected void GetData()
        {
            string sql = string.Empty;
            sql = string.Format("select 成绩表.学号,姓名,课程号,成绩,性别,班级名  from 成绩表 join 学生信息表 on 成绩表.学号 = 学生信息表.学号");
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds, "t");
                this.DataGrid1.DataSource = ds.Tables["t"];
                this.DataGrid1.DataBind();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
        protected void Button1_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("AdminGradeAdd.aspx");
        }
    }
}