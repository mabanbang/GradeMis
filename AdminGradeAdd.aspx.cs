﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GradeMis
{
    public partial class AdminGradeAdd : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["GradeSys"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            // 在此处放置用户代码以初始化页面
            GetClassName();
            try
            {
                if (Request.QueryString["Action"].ToString() == "Add")
                {
                    GetStu();
                }
            }
            catch
            { }
        }
        protected void GetClassName()
        {
            string sql = string.Empty;
            sql = string.Format("select 班级名 from 学生信息表 group by 班级名");
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds, "t");
                this.DataGrid1.DataSource = ds.Tables["t"];
                this.DataGrid1.DataBind();
            }
            catch
            { }
        }
        protected void GetStu()
        {
            string sql = string.Empty;
            sql = string.Format("select 学号,姓名,性别,班级名,所在院系 from 学生信息表 where 班级名='{0}'", Request.QueryString["班级名"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds, "t");
                this.DataGrid2.DataSource = ds.Tables["t"];
                this.DataGrid2.DataBind();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
        protected void DataGrid1_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            this.DataGrid1.CurrentPageIndex = e.NewPageIndex;
            GetClassName();

        }

        protected void DataGrid2_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            this.DataGrid2.CurrentPageIndex = e.NewPageIndex;
            GetStu();

        }
    }
}