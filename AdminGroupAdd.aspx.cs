﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GradeMis
{
    public partial class AdminGroupAdd : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["GradeSys"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }
        #region
        protected void AddGroup()
        {
            string sql = string.Empty;
            sql = string.Format("insert into AdminGroup([group],comment) values('{0}','{1}')", this.txtGname.Text, this.txtGinfo.Text);
            SqlCommand cmd = new SqlCommand(sql, conn);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                Response.Redirect("AdminGroup.aspx");
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion
        protected void Button1_Click(object sender, System.EventArgs e)
        {
            AddGroup();

        }
    }
}