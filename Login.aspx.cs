﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GradeMis
{
    public partial class Login : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["GradeSys"]);

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #region
        protected void userLogin()
        {
            string sql = string.Empty;
            string user = this.txtUsername.Text;
            string pwd = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(this.txtPassword.Text, "MD5");
            sql = string.Format("select username,password,[group] from admin where username='{0}' and password='{1}'", user, pwd);
            SqlDataReader dr;
            SqlCommand cmd = new SqlCommand(sql, conn);
            try
            {
                conn.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    Session["username"] = dr["username"].ToString();
                    Session["group"] = dr["group"].ToString();
                    Response.Redirect("Main.aspx");
                }
                else
                {
                    Response.Write("<script>alert('用户名或者密码错误,请重新输入!')</script>");
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion
    
        protected void Button1_Click1(object sender, EventArgs e)
        {
            userLogin();
        }
    }
}