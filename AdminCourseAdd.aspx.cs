﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GradeMis
{
    public partial class AdminCourseAdd : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["GradeSys"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }
        #region
        protected void Add()
        {
            string sql = string.Empty;
            sql = string.Format("insert into 课程表(课程号,课程名,先修课) values('{0}','{1}','{2}')", this.txtid.Text, this.txtname.Text, this.txtpre.Text);
            SqlCommand cmd = new SqlCommand(sql, conn);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                Response.Write("<script>alert('课程信息添加成功!')</script>");
            }
            catch//(Exception ex)
            {
                //Response.Write(ex);
                Response.Write("<script>alert('课程信息添加失败!')</script>");
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        protected void Button1_Click(object sender, System.EventArgs e)
        {
            Add();
        }

        protected void Button2_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("AdminCourse.aspx");
        }
    }
}