﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GradeMis
{
    public partial class AdminStuStat : System.Web.UI.Page
    {
          protected SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["GradeSys"]);
      

        protected void Page_Load(object sender, EventArgs e)
        {
            GetData();
        }
        #region 取得数据
          protected void GetData()
        {
            string sql = string.Empty;
            sql = string.Format("select 课程号,sum(成绩) as 总分,avg(成绩) as 平均分,count(学号) as 人数 from v_stuGrade group by 课程号");
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds, "t");
                this.DataGrid1.DataSource = ds.Tables["t"];
                this.DataGrid1.DataBind();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
        #endregion
          protected void DataGrid1_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            this.DataGrid1.CurrentPageIndex = e.NewPageIndex;
            GetData();
        }
    }
}